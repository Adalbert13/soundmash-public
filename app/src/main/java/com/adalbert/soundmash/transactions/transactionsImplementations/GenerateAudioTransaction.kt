package com.adalbert.soundmash.transactions.transactionsImplementations

import com.adalbert.soundmash.generators.AudioGenerator
import com.adalbert.soundmash.models.ITrack
import com.adalbert.soundmash.connections.AudioDestination
import com.adalbert.soundmash.connections.AudioInfo
import com.adalbert.soundmash.connections.AudioMiddleman
import com.adalbert.soundmash.transactions.AudioTransaction

class GenerateAudioTransaction(private val generator : AudioGenerator, private val mTrack : ITrack, private val offset : Int) : AudioTransaction {

    private var mAudioMiddleman : AudioMiddleman? = null
    override var audioMiddleman: AudioMiddleman?
        get() = mAudioMiddleman
        set(value) {
            value?.let { mAudioMiddleman = value }
        }
    override val usedCacheSize: Long
        get() = 0

    private var loadedFrames = 0

    override fun apply() {
        generator.generateAudio(object : AudioDestination {
            override val desiredAudioInfo = AudioInfo(mTrack.samplingRate, mTrack.validBits, mTrack.numChannels)

            override fun openDestination() {}
            override fun closeDestination() {}

            override fun saveBuffer(audioBuffer: Array<FloatArray>, actualAudioInfo : AudioInfo, offset : Int, framesToLoad : Int) {
                mTrack.bulkInsertSamples(audioBuffer, mTrack.samplingRate, mTrack.validBits, offset, framesToLoad)
                loadedFrames += framesToLoad
            }
            override fun notifyDataSize(additionalSize: Int, numChannels: Int) {
                mTrack.assureSufficientCapacity(additionalSize, numChannels)
            }
            override fun notifyFinishOperation() {
                audioMiddleman?.operationFinished(true)
            }
        },  offset)
    }

    override fun revert() {
        mTrack.removeSamples(offset, offset + loadedFrames)
        audioMiddleman?.operationFinished(true)
    }
}