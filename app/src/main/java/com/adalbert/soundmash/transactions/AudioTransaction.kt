package com.adalbert.soundmash.transactions

import com.adalbert.soundmash.connections.AudioMiddleman

/**
 * Audio transactions implementation provide simple applying and reverting of operations performed on audio.
 */
interface AudioTransaction {
    /**
     * An optional audio middleman is used to perform actions on audio transaction's finish.
     */
    var audioMiddleman : AudioMiddleman?

    /**
     * An optional parameter, showing how much cache (in bytes) will be approximately used by the transaction in cache memory.
     */
    val usedCacheSize : Long?

    fun apply()
    fun revert()
}