package com.adalbert.soundmash.transactions.transactionsImplementations

import android.util.Log
import cafe.adriel.androidaudioconverter.model.AudioFormat
import com.adalbert.soundmash.audioio.AudioIO
import com.adalbert.soundmash.connections.AudioMiddleman
import com.adalbert.soundmash.effects.AudioEffect
import com.adalbert.soundmash.models.ITrack
import com.adalbert.soundmash.transactions.*
import com.adalbert.soundmash.connections.connectionsImplementations.TrackCopyAudioSource
import com.adalbert.soundmash.connections.connectionsImplementations.LogAudioMiddleman
import com.adalbert.soundmash.connections.connectionsImplementations.TrackReplaceAudioDestination

class ApplyEffectTransaction(private val effect : AudioEffect, private val mTrack : ITrack, private val cachePath : String, private val offset : Int, private val framesToLoad : Int) : AudioTransaction {

    private var mAudioMiddleman : AudioMiddleman? = null
    override var audioMiddleman: AudioMiddleman?
        get() = mAudioMiddleman
        set(value) {
            value?.let { mAudioMiddleman = value }
        }

    override val usedCacheSize: Long? = framesToLoad.toLong() * (mTrack.validBits / 8) * mTrack.numChannels

    override fun apply() {
        AudioIO.AudioWriterInstance.writeFrames(cachePath, AudioFormat.WAV, TrackCopyAudioSource(mTrack), object : AudioMiddleman {
            override fun operationFinished(success: Boolean, message: String?, throwable: Throwable?) {
                // if cache file rightfully saved, start the effect
                when (success) {
                    true -> effect.streamDataToEffect(TrackCopyAudioSource(mTrack), TrackReplaceAudioDestination(mTrack, audioMiddleman), audioMiddleman = audioMiddleman, offset = offset, framesToAlter = framesToLoad)
                    false -> {
                        Log.e("EFFECT_TRANSACTION_CACHE_SAVE", message, throwable)
                        audioMiddleman?.operationFinished(false, message, throwable)
                    }
                }
            }
        }, offset, framesToLoad)

    }

    override fun revert() {
        AudioIO.AudioReaderInstance.readFrames("$cachePath.wav", 0, offset, null, TrackReplaceAudioDestination(mTrack, mAudioMiddleman), LogAudioMiddleman())
    }
}