package com.adalbert.soundmash.gestures

import android.view.GestureDetector
import android.view.MotionEvent
import android.view.ScaleGestureDetector

/**
 * A general gesture listener, containing universal methods describing supported touch gestures.
 */
interface CompleteGestureListener : GestureDetector.OnGestureListener, ScaleGestureDetector.OnScaleGestureListener, GestureDetector.OnDoubleTapListener {

    override fun onDown(e: MotionEvent?): Boolean { return false }

    override fun onShowPress(e: MotionEvent?) { }

    override fun onSingleTapUp(e: MotionEvent?): Boolean { return true }

    override fun onScroll(e1: MotionEvent?, e2: MotionEvent?, distanceX: Float, distanceY: Float): Boolean { return true }

    override fun onLongPress(e: MotionEvent?) { }

    override fun onFling(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float): Boolean { return true }

    override fun onScale(detector: ScaleGestureDetector?): Boolean { return true }

    override fun onScaleBegin(detector: ScaleGestureDetector?): Boolean { return true }

    override fun onScaleEnd(detector: ScaleGestureDetector?) { }

    override fun onSingleTapConfirmed(e: MotionEvent?): Boolean { return true }

    override fun onDoubleTap(e: MotionEvent?): Boolean { return true }

    override fun onDoubleTapEvent(e: MotionEvent?): Boolean { return true }

}