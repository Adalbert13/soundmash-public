package com.adalbert.soundmash.services

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.media.AudioRecord
import android.media.AudioTrack
import android.media.MediaRecorder
import android.util.Log
import cafe.adriel.androidaudioconverter.model.AudioFormat
import com.adalbert.soundmash.R
import com.adalbert.soundmash.audioio.AudioIO
import com.adalbert.soundmash.presenters.WaveBar
import com.adalbert.soundmash.connections.AudioInfo
import com.adalbert.soundmash.connections.AudioMiddleman
import com.adalbert.soundmash.connections.connectionsImplementations.ArrayCopyAudioSource
import com.codekidlabs.storagechooser.StorageChooser
import java.lang.IllegalStateException
import java.text.DateFormat
import java.util.*

class AudioRecorder(private val maxRecordingLengthSamples : Int = 2 * 5 * 60 * 44100, var recordingFinishedAudioMiddleman: AudioMiddleman,
                    onBufferRecorder : (Array<WaveBar>) -> Unit = {_ -> }, onRecordingSpaceFinished : () -> Unit) {

    private var mSamplingRate = 44100
    private var mValidBits = 16
    private var numChannels = 2

    val samplingRate : Int
        get() { return mSamplingRate }

    val validBits : Int
        get() { return mValidBits }

    private lateinit var audioData : Array<FloatArray>
    private lateinit var audioBuffer : FloatArray
    private var bufferSizeInFloats : Int = 0

    private lateinit var audioRecord : AudioRecord
    private var mFramesRecorded = 0

    val framesRecorded : Int
        get() { return mFramesRecorded }

    private var mRecordingThread: Thread? = null
    private var mIsInterrupted = false
    private val mAllowedTimeToSleep = 2000

    private var mIsReady = false
    val isReady : Boolean
        get() { return mIsReady }

    private var mIsDataReady = false
    val isDataReady : Boolean
        get() { return mIsDataReady }

    private val recordingRunnable = Runnable {
        if (!mIsReady) {
            recordingFinishedAudioMiddleman.operationFinished(false, "Couldn't start the recording process!", IllegalStateException("Recorder isn't ready for operation!"))
            return@Runnable
        }

        audioRecord.startRecording()
        mFramesRecorded = 0
        while (mFramesRecorded + bufferSizeInFloats < maxRecordingLengthSamples && !mIsInterrupted) {
            Log.d("Frames recorded", "Recorder $mFramesRecorded out of $maxRecordingLengthSamples")
            val previouslyRecorded = mFramesRecorded
            mFramesRecorded += audioRecord.read(audioBuffer, 0, bufferSizeInFloats, AudioRecord.READ_BLOCKING)
            if (audioRecord.channelCount == 1) {
                onBufferRecorder(Array (1) { WaveBar(0f, 0f, audioBuffer.max()!!, 0f) })
                audioBuffer.copyInto(audioData[0], previouslyRecorded, 0, mFramesRecorded - previouslyRecorded)
            } else {
                val maxes = FloatArray(2) { -Float.MAX_VALUE }
                for (sampleIndex in 0 until mFramesRecorded - previouslyRecorded) {
                    val channelIndex = sampleIndex % audioRecord.channelCount
                    val frameIndex = previouslyRecorded / 2 + sampleIndex / audioRecord.channelCount
                    audioData[channelIndex][frameIndex] = audioBuffer[sampleIndex]
                    if (audioBuffer[sampleIndex] > maxes[channelIndex])
                        maxes[channelIndex] = audioBuffer[sampleIndex]
                }
                onBufferRecorder(Array (2) {index : Int -> WaveBar(0f, 0f, maxes[index], 0f) })
            }

        }
        if (mFramesRecorded + bufferSizeInFloats >= maxRecordingLengthSamples)
            onRecordingSpaceFinished()
        audioRecord.stop()
        audioRecord.release()
    }

    fun prepareRecorder(audioInfo : AudioInfo) : Boolean {
        this.mSamplingRate = audioInfo.samplingRate
        this.mValidBits = audioInfo.validBits
        this.numChannels = audioInfo.numChannels

        val channelAmount = if (numChannels == 2) 2 else 1

        val channelConfig = when (channelAmount) {
            2 -> android.media.AudioFormat.CHANNEL_IN_STEREO
            else -> android.media.AudioFormat.CHANNEL_IN_MONO
        }

        bufferSizeInFloats = AudioTrack.getMinBufferSize(mSamplingRate, channelConfig,
            android.media.AudioFormat.ENCODING_PCM_FLOAT
        )

        if (bufferSizeInFloats == AudioTrack.ERROR || bufferSizeInFloats == AudioTrack.ERROR_BAD_VALUE) {
            bufferSizeInFloats = mSamplingRate * numChannels
        }

        audioBuffer = FloatArray(bufferSizeInFloats)

        audioData = Array(channelAmount) { FloatArray(maxRecordingLengthSamples / channelAmount) }

        audioRecord = AudioRecord(
            MediaRecorder.AudioSource.DEFAULT, mSamplingRate,
            channelConfig, android.media.AudioFormat.ENCODING_PCM_FLOAT, bufferSizeInFloats * 4)

        mIsReady = when (audioRecord.state != AudioRecord.STATE_INITIALIZED) {
            true -> {
                Log.e("AUDIO_RECORDER", "AudioRecord couldn't be initialized!")
                recordingFinishedAudioMiddleman.operationFinished(false, "Couldn't initialize the recording process!", IllegalStateException("Recorder couldn't be initialized!"))
                false
            }
            else -> true
        }

        return mIsReady
    }

    private fun blockingStopRecording() : Boolean {
        var timeSlept = 0
        mIsInterrupted = true
        while (mRecordingThread?.isAlive == true) {
            timeSlept += 10
            Thread.sleep(10)
            if(timeSlept > mAllowedTimeToSleep) {
                Log.e("AUDIO_RECORDER", "A recording thread operates in the background, couldn't have been stopped!")
                return false
            }
        }
        mIsInterrupted = false
        return true
    }

    fun startRecording() {
        Thread {
            val successfullyStopped = blockingStopRecording()
            if (successfullyStopped) {
                mRecordingThread = Thread(recordingRunnable)
                mRecordingThread!!.start()
            } else {
                recordingFinishedAudioMiddleman.operationFinished(false, "Audio recording couldn't be started!", IllegalStateException("Previous thread wasn't successfully stopped!"))
            }
        }.start()
    }

    fun stopRecording() {
        Thread {
            val successfullyStopped = blockingStopRecording()
            if (successfullyStopped && mFramesRecorded > 0) {
                mIsDataReady = true
                recordingFinishedAudioMiddleman.operationFinished(true)
            } else {
                recordingFinishedAudioMiddleman.operationFinished(false, "Audio recording wasn't successful!", IllegalStateException("Previous thread wasn't successfully stopped, or data size is 0!"))
            }
            mIsReady = false
        }.start()
    }

    fun saveData(activity: Activity, onDataSaved : (filePath: String?) -> Unit) {
        if (!mIsDataReady) {
            recordingFinishedAudioMiddleman.operationFinished(false, "Audio recording wasn't successful!", IllegalStateException("Recorder doesn't have the needed data!"))
            return
        }
        mIsDataReady = false
        val storageChooser = StorageChooser.Builder()
            .withActivity(activity)
            .withFragmentManager(activity.fragmentManager)
            .withMemoryBar(true)
            .allowCustomPath(true)
            .setType(StorageChooser.DIRECTORY_CHOOSER)
            .build()

        storageChooser.setOnSelectListener {
            val builder = AlertDialog.Builder(activity)
            var progressDialog : Dialog? = null
            builder.setMessage("Writing audio...")
            activity.runOnUiThread() {
                progressDialog = builder.create()
                progressDialog?.setCanceledOnTouchOutside(false)
                progressDialog?.show()
            }
            val dateFormat = DateFormat.getDateTimeInstance()
            val filePath = "$it/Recording ${dateFormat.format(Calendar.getInstance().time)}"
            AudioIO.AudioWriterInstance.writeFrames(filePath, AudioFormat.WAV,
                ArrayCopyAudioSource(audioData, samplingRate, validBits, framesRecorded / numChannels),
                audioMiddleman = object : AudioMiddleman {
                    override fun operationFinished(success: Boolean, message: String?, throwable: Throwable?) {
                        Log.e("AUDIO_WRITE", message, throwable)
                        progressDialog?.dismiss()
                        if (!success) {
                            builder.setTitle("Audio writing error")
                                .setMessage(message)
                                .setIcon(R.drawable.error)
                            activity.runOnUiThread() { builder.create().show() }
                        } else
                            onDataSaved("$filePath.wav")
                    }
                }
            )
        }
        activity.runOnUiThread { storageChooser.show() }
    }

}