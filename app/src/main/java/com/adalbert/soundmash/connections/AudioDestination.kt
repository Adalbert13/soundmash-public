package com.adalbert.soundmash.connections

/**
 * The interface is used to describe an audio destination - an object, which can consume audio data.
 */
interface AudioDestination {
    /**
     * Audio parameters desired by the audio destination. Read more in the [AudioInfo] documentation.
     */
    val desiredAudioInfo : AudioInfo

    /**
     * Opens the audio destination, useful for eg. files support.
     */
    fun openDestination()

    /**
     * Notifies the audio destination of the additional size of incoming audio frames amount.
     * @param additionalSize Number of additional audio frames, that audio destination needs to make room for
     * @param numChannels Amount of channels of incoming data
     */
    fun notifyDataSize(additionalSize : Int, numChannels : Int)

    /**
     * Saves the buffer's contents in the audio destination.
     * @param audioBuffer Multi-channel buffer for audio data storage
     * @param actualAudioInfo The actual audio parameters of incoming data
     * @param offset Describes, where to put incoming data in the audio destination
     * @param framesToLoad Describes, how many frames are going to be loaded
     */
    fun saveBuffer(audioBuffer : Array<FloatArray>, actualAudioInfo: AudioInfo, offset : Int, framesToLoad : Int)

    /**
     * Notifies audio destination about finishing the audio operation.
     */
    fun notifyFinishOperation()

    /**
     * Closes the audio destination, useful for eg. files support.
     */
    fun closeDestination()
}