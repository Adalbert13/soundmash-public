package com.adalbert.soundmash.connections.connectionsImplementations

import com.adalbert.soundmash.models.ITrack
import com.adalbert.soundmash.connections.AudioInfo
import com.adalbert.soundmash.connections.AudioSource

class TrackCopyAudioSource(private val mTrack : ITrack) : AudioSource {

    override val audioInfo = AudioInfo(mTrack.samplingRate, mTrack.validBits, mTrack.numChannels)
    override val numFrames = mTrack.numFrames

    override fun openSource() {}
    override fun closeSource() {}

    override fun loadBuffer(audioBuffer: Array<FloatArray>, offset: Int, framesToLoad: Int) {
        mTrack.getCopyOfSamples(audioBuffer, offset, offset + framesToLoad)
    }

}