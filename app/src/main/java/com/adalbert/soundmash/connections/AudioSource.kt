package com.adalbert.soundmash.connections

/**
 * The interface is used to describe an audio source - an object, which can deliver audio data.
 */
interface AudioSource {
    /**
     * Parameters of the delivered audio. Read more in [AudioInfo] documentation.
     */
    val audioInfo : AudioInfo

    /**
     * Number of frames, that can be delivered in total by the audio source.
     */
    val numFrames : Int

    /**
     * Opens the audio source, useful for eg. files support.
     */
    fun openSource()

    /**
     * Loads the desired frames from audio source into [audioBuffer].
     * @param audioBuffer Multi-channel buffer for audio data storage
     * @param offset Describes, where to get frames data from
     * @param framesToLoad Describes, how many frames are going to be loaded
     */
    fun loadBuffer(audioBuffer : Array<FloatArray>, offset : Int, framesToLoad : Int)

    /**
     * Closes the audio source, useful for eg. files support.
     */
    fun closeSource()
}