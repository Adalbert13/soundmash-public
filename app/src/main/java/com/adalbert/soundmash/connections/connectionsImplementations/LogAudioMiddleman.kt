package com.adalbert.soundmash.connections.connectionsImplementations

import android.util.Log
import com.adalbert.soundmash.connections.AudioMiddleman

class LogAudioMiddleman : AudioMiddleman {
    override fun operationFinished(success: Boolean, message: String?, throwable: Throwable?) {
        if (!success) Log.e("EMPTY_AUDIO_MIDDLEMAN", message, throwable)
    }

}