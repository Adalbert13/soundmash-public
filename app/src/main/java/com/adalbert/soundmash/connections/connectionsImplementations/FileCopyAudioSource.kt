package com.adalbert.soundmash.connections.connectionsImplementations

import com.adalbert.soundmash.connections.AudioInfo
import com.adalbert.soundmash.connections.AudioSource
import edu.illinois.cs.cs125.lib.wavfile.WavFile
import java.io.File

class FileCopyAudioSource(private val filePath : String) : AudioSource {

    private var wavFile : WavFile? = null

    override val audioInfo: AudioInfo
        get() {
            wavFile?.let {
                return AudioInfo(it.sampleRate.toInt(), it.validBits, it.numChannels)
            }
            val wavFile = WavFile.openWavFile(File(filePath))
            val output = AudioInfo(wavFile.sampleRate.toInt(), wavFile.validBits, wavFile.numChannels)
            wavFile.close()
            return output
        }

    override val numFrames: Int
        get() {
            wavFile?.let {
                return it.numFrames.toInt()
            }
            val wavFile = WavFile.openWavFile(File(filePath))
            val output = wavFile.numFrames
            wavFile.close()
            return output.toInt()
        }

    override fun openSource() {
        if (wavFile == null)
            wavFile = WavFile.openWavFile(File(filePath))
    }

    override fun loadBuffer(audioBuffer: Array<FloatArray>, offset: Int, framesToLoad: Int) {
        wavFile!!.readFrames(audioBuffer, framesToLoad)
    }

    override fun closeSource() {
        wavFile?.let {
            it.close()
            wavFile = null
        }
    }

}