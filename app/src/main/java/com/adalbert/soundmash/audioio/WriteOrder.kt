package com.adalbert.soundmash.audioio

import cafe.adriel.androidaudioconverter.model.AudioFormat
import com.adalbert.soundmash.connections.AudioMiddleman
import com.adalbert.soundmash.connections.AudioSource

class WriteOrder(val filePath: String, val format: AudioFormat,
                 val audioSource: AudioSource, val audioMiddleman: AudioMiddleman,
                 val offset: Int, val framesToWrite: Int)