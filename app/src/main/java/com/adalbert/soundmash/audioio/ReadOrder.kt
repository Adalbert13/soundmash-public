package com.adalbert.soundmash.audioio

import com.adalbert.soundmash.connections.AudioDestination
import com.adalbert.soundmash.connections.AudioMiddleman

class ReadOrder(val filePath: String, val offsetSource: Int, val offsetDestination : Int, val framesToLoad: Int?,
                val audioDestination: AudioDestination, val audioMiddleman: AudioMiddleman?
)