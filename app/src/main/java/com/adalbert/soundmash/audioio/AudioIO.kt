package com.adalbert.soundmash.audioio

import android.content.Context
import cafe.adriel.androidaudioconverter.AndroidAudioConverter
import cafe.adriel.androidaudioconverter.callback.ILoadCallback
import java.lang.Exception

class AudioIO private constructor() {

    companion object : ILoadCallback {
        private lateinit var appContext : Context
        private var mIsFFmpegSupported : Boolean? = null
        val isFFMPEGInitialized : Boolean
            get() = mIsFFmpegSupported != null
        val isFFMPEGSupported : Boolean
            get() = mIsFFmpegSupported ?: false

        private lateinit var mAudioReader : AudioReader
        private lateinit var mAudioWriter : AudioWriter
        val AudioReaderInstance : AudioReader
            get() = mAudioReader
        val AudioWriterInstance : AudioWriter
            get() = mAudioWriter

        fun initializeInstance(appContext: Context) {
            this.appContext = appContext
            this.mAudioReader = AudioReader.getInstance(appContext)
            this.mAudioWriter = AudioWriter.getInstance(appContext)
            AndroidAudioConverter.load(appContext, this)
        }

        override fun onSuccess() {
            mIsFFmpegSupported = true
        }

        override fun onFailure(error: Exception?) {
            mIsFFmpegSupported = false
        }
    }

}