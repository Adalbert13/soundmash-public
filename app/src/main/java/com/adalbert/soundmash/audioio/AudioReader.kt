package com.adalbert.soundmash.audioio

import android.content.Context
import android.util.Log
import cafe.adriel.androidaudioconverter.AndroidAudioConverter
import cafe.adriel.androidaudioconverter.callback.IConvertCallback
import cafe.adriel.androidaudioconverter.model.AudioFormat
import com.adalbert.soundmash.connections.AudioDestination
import com.adalbert.soundmash.connections.AudioInfo
import com.adalbert.soundmash.connections.AudioMiddleman
import com.adalbert.soundmash.utils.getFileType
import com.adalbert.soundmash.utils.sSupportedAudioFormats
import edu.illinois.cs.cs125.lib.wavfile.WavFile
import java.io.File
import java.io.FileNotFoundException
import java.lang.IllegalArgumentException
import java.util.*

class AudioReader
    private constructor(private val appContext: Context) {

    private val mOrdersQueue: MutableList<ReadOrder> = Collections.synchronizedList(mutableListOf())

    @get:Synchronized @set:Synchronized
    private var isThreadAlive = false

    companion object {
        private var instance : AudioReader? = null

        fun getInstance(appContext: Context) : AudioReader {
            if (instance == null)
                instance = AudioReader(appContext)
            return instance!!
        }
    }

    private val handleOrders = Runnable {
        while (mOrdersQueue.size > 0) {
            val readOrder = mOrdersQueue.removeAt(0)
            val file = File(readOrder.filePath)
            if (!file.exists())
                readOrder.audioMiddleman?.operationFinished(
                    false, "The requested file (${file.name}) doesn't exist!", FileNotFoundException(file.name)
                )
            val fileType = getFileType(readOrder.filePath)
            if (sSupportedAudioFormats.contains(fileType)) {
                when {
                    fileType == "WAV" -> readFromWav(file, readOrder.offsetSource, readOrder.offsetDestination, readOrder.framesToLoad, readOrder.audioDestination, readOrder.audioMiddleman)
                    AudioIO.isFFMPEGSupported -> {
                        convertToWav(file, object : IConvertCallback {
                            override fun onSuccess(convertedFile: File?) {
                                convertedFile?.let { readFromWav(convertedFile, readOrder.offsetSource, readOrder.offsetDestination, readOrder.framesToLoad, readOrder.audioDestination, readOrder.audioMiddleman) }
                                if (convertedFile == null)  readOrder.audioMiddleman?.operationFinished(false, "Couldn't convert requested file!", IllegalStateException())
                            }
                            override fun onFailure(error: java.lang.Exception?) {
                                readOrder.audioMiddleman?.operationFinished(false, "Couldn't convert requested file!", error)
                            }
                        })
                    }
                    else -> readOrder.audioMiddleman?.operationFinished(false, "Audio conversion is not supported on your system!", IllegalStateException())
                }
            } else {
                readOrder.audioMiddleman?.operationFinished(false, "This audio format isn't supported!", IllegalArgumentException(fileType))
            }
        }
    }

    fun readFrames(filePath: String, offsetSource: Int = 0, offsetDestination : Int = 0, framesToRead: Int?,
                   audioDestination: AudioDestination, audioMiddleman: AudioMiddleman?) {
        mOrdersQueue.add(ReadOrder(filePath, offsetSource, offsetDestination, framesToRead, audioDestination, audioMiddleman))
        if (!isThreadAlive) {
            Thread {
                isThreadAlive = true
                handleOrders.run()
                isThreadAlive = false
            }.start()
        }
    }

    private fun readFromWav(file: File, offsetSource: Int, offsetDestination : Int, framesToRead : Int?,
                            audioDestination: AudioDestination, audioMiddleman: AudioMiddleman?) {
        val startTime = System.currentTimeMillis()
        val bufferSize = 4096
        var wavFile : WavFile? = null
        try {
            wavFile = WavFile.openWavFile(file)
        } catch (ex : Exception) {
            audioMiddleman?.operationFinished(false, "The supplied file is damaged!", ex)
        }
        wavFile?.let { wavFile ->
            val actualAudioInfo = AudioInfo(wavFile.sampleRate.toInt(), wavFile.validBits, wavFile.numChannels)

            val message : String? = when {
                audioDestination.desiredAudioInfo.samplingRate != 0 && wavFile.sampleRate.toInt() != audioDestination.desiredAudioInfo.samplingRate -> "Sampling rate doesn't match already present audio!"
                audioDestination.desiredAudioInfo.validBits != 0 && wavFile.validBits != audioDestination.desiredAudioInfo.validBits -> "Bit depth doesn't match already present audio!"
                else -> null
            }
            message?.let {
                audioMiddleman?.operationFinished(false, it, IllegalArgumentException(it))
                return
            }

            val buffer = Array(wavFile.numChannels) { FloatArray(bufferSize) }
            // Fast-forwarding to given offset
            iterateOverFrames(wavFile, buffer, 0, offsetSource)
            val actualFramesToRead = when (framesToRead) {
                null -> wavFile.framesRemaining.toInt()
                else -> framesToRead
            }
            audioDestination.notifyDataSize(actualFramesToRead, wavFile.numChannels)
            val loadToAudioReceiverFun = { readFrames : Array<FloatArray>, innerOffsetDestination : Int, framesToLoad : Int ->
                audioDestination.saveBuffer(readFrames, actualAudioInfo, innerOffsetDestination, framesToLoad)
            }
            iterateOverFrames(wavFile, buffer, offsetDestination, actualFramesToRead, loadToAudioReceiverFun)
            wavFile.close()
            audioDestination.notifyFinishOperation()
            Log.d("WAVFILE", "WAVFILE READ IN ${System.currentTimeMillis() - startTime} ms")
            Log.d("WAVFILE", "WAVFILE CLOSED")
        }
    }

    // TODO : Change the creation of additionalFrames to calculating framesToLoad
    private fun iterateOverFrames(wavFile : WavFile, buffer : Array<FloatArray>, offsetDestination : Int, framesToIterate : Int,
                                  readFramesAction : (readFrames : Array<FloatArray>, offsetDestination : Int, framesToLoad : Int) -> Unit = {_, _, _ -> }) {
        var readFrames = 0
        val bufferSize = buffer[0].size
        while (wavFile.framesRemaining > 0 && readFrames + bufferSize < framesToIterate) {
            wavFile.readFrames(buffer, bufferSize)
            readFramesAction(buffer, offsetDestination + readFrames, bufferSize)
            readFrames += bufferSize
        }
        val remainingFrames = framesToIterate - readFrames
        if (remainingFrames > 0) {
            val additionalFrames = Array(wavFile.numChannels) { FloatArray(remainingFrames) }
            wavFile.readFrames(additionalFrames, remainingFrames)
            readFramesAction(additionalFrames, offsetDestination + readFrames, remainingFrames)
        }
    }

    private fun convertToWav(file: File, callback: IConvertCallback) {
        AndroidAudioConverter.with(appContext)
            .setFile(file)
            .setFormat(AudioFormat.WAV)
            .setCallback(callback)
            .convert()
    }

}
