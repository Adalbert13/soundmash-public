package com.adalbert.soundmash.models

/**
 * Track listener is an entity that should be informed of the changes taking place in connected ITrack implementation.
 */
interface TrackListener {
    fun dataUpdated()
}