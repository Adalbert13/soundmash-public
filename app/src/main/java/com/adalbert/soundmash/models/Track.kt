package com.adalbert.soundmash.models

class Track : ITrack {

    private val mListenersList : MutableList<TrackListener> = mutableListOf()
    private val mChannelsData : MutableList<Channel> = mutableListOf(Channel())
    private var mLastSampleIndex : Int = 0
    private var mSamplingRate : Int? = null
    private var mValidBits : Int? = null

    override val numFrames: Int
        get() = mLastSampleIndex

    // TODO : Maybe change that to null
    override val samplingRate : Int
        get() = mSamplingRate ?: 0

    override val numChannels : Int
        get() = mChannelsData.size

    // TODO : Maybe change that to null
    override val validBits: Int
        get() = mValidBits ?: 0

    @Throws(IllegalArgumentException::class)
    override fun insertSamples(channelSamples : Array<FloatArray>, samplingRate: Int, validBits : Int, index : Int, samplesToLoad : Int) {
        bulkInsertSamples(channelSamples, samplingRate, validBits, index, samplesToLoad)
        updateTrackChangesListeners()
    }

    @Throws(IllegalArgumentException::class)
    override fun bulkInsertSamples(channelSamples : Array<FloatArray>, samplingRate: Int, validBits : Int, index : Int, samplesToLoad : Int) {
        if (mSamplingRate != null && mSamplingRate != samplingRate)
            throw IllegalArgumentException("Wrong sampling rate of the sound!")
        else if (mSamplingRate == null)
            mSamplingRate = samplingRate

        if (mValidBits != null && mValidBits != validBits)
            throw IllegalArgumentException("Wrong bit depth of the sound!")
        else if (mValidBits == null)
            mValidBits = validBits

        val channelsAmount = channelSamples.size
        if (channelsAmount > mChannelsData.size)
            copyExistingToAdditionalChannels(channelsAmount)
        for (channelIndex in mChannelsData.indices)
            mChannelsData[channelIndex].insertSamples(channelSamples[channelIndex % channelSamples.size], index, samplesToLoad)
        mLastSampleIndex = mChannelsData[0].samplesSize
    }

    override fun packSamples(channelSamples: Array<FloatArray>, samplingRate: Int, validBits: Int, samplesToLoad: Int) {
        clearTrack()
        mSamplingRate = samplingRate
        mValidBits = validBits
        for (channelIndex in channelSamples.indices) {
            mChannelsData.add(Channel())
            mChannelsData[channelIndex].packSamples(channelSamples[channelIndex % channelSamples.size], samplesToLoad)
        }
        mLastSampleIndex = mChannelsData[0].samplesSize
        updateTrackChangesListeners()
    }

    override fun getOriginalSamples() : Array<FloatArray> {
        val samples = Array(numChannels) { FloatArray(0) }
        for (channelIndex in mChannelsData.indices)
            samples[channelIndex] = mChannelsData[channelIndex].getOriginalSamples()
        return samples
    }

    override fun getCopyOfSamples(buffer : Array<FloatArray>, startIndex : Int, endIndex : Int) {
        for (channelIndex in mChannelsData.indices)
            mChannelsData[channelIndex].getCopyOfSamples(buffer[channelIndex], startIndex, endIndex)
    }

    override fun getCopyOfSamplesInSingleArray(buffer : FloatArray, startIndex : Int, endIndex : Int) {
        for (channelIndex in mChannelsData.indices)
            mChannelsData[channelIndex].getCopyOfSamplesInSingleArray(buffer, channelIndex, mChannelsData.size, startIndex, endIndex)
    }

    override fun assureSufficientCapacity(additionalDataSize : Int, amountOfChannels : Int) {
        copyExistingToAdditionalChannels(amountOfChannels)
        for (channel in mChannelsData)
            channel.assureSufficientCapacity(additionalDataSize)
    }

    override fun removeSamples(startIndex : Int, endIndex : Int) {
        bulkRemoveSamples(startIndex, endIndex)
        updateTrackChangesListeners()
    }

    override fun bulkRemoveSamples(startIndex: Int, endIndex: Int) {
        for (channel in mChannelsData)
            channel.removeSamples(startIndex, endIndex)
        mLastSampleIndex = mChannelsData[0].samplesSize
    }

    override fun replaceSamples(buffer: Array<FloatArray>, offset: Int, framesToReplace: Int) {
        mChannelsData.forEachIndexed {index, channel ->  channel.replaceSamples(buffer[index], offset, framesToReplace) }
    }

    override fun moveSamples(sourceStartIndex : Int, sourceEndIndex : Int, destinationIndex : Int) {
        mChannelsData.forEach { it.moveSamples(sourceStartIndex, sourceEndIndex, destinationIndex) }
        mLastSampleIndex = mChannelsData[0].samplesSize
        updateTrackChangesListeners()
    }

    private fun copyExistingToAdditionalChannels(newChannelsAmount : Int) {
        val oldChannelsAmount = mChannelsData.size
        for (channelIndex in oldChannelsAmount until newChannelsAmount) {
            mChannelsData.add(Channel())
            if (channelIndex > 0 && mChannelsData[channelIndex - 1].samplesSize != 0)
                mChannelsData[channelIndex].insertSamples(mChannelsData[channelIndex - 1].getCopyOfSamples())
        }
    }

    override fun clearTrack() {
        for (channel in mChannelsData)
            channel.clearChannel()
        mChannelsData.clear()
        mLastSampleIndex = 0
        mSamplingRate = null
        mValidBits = null
    }

    /*********************************************************
     **********  <<  LISTENERS UTILITY METHODS  >>  **********
     *********************************************************/

    override fun registerListener(listener: TrackListener) {
        mListenersList.add(listener)
    }

    override fun unregisterListener(listener: TrackListener) {
        mListenersList.remove(listener)
    }

    override fun updateTrackChangesListeners() {
        for (listener in mListenersList)
            listener.dataUpdated()
    }
}