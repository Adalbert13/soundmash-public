package com.adalbert.soundmash.views

import android.view.View
import com.adalbert.soundmash.gestures.CompleteGestureDetector
import com.adalbert.soundmash.gestures.CompleteGestureListener
import com.adalbert.soundmash.gestures.GestureContext
import com.adalbert.soundmash.views.renderables.Renderable

/**
 * The interface describes track view, contains methods and properties necessary in interaction with presenters.
 */
interface ITrackView {
    /**
     * Used in the manipulation of rendered content.
     */
    var mRenderingBeginIndex : Int

    /**
     * Describes bar width in pixels.
     */
    var mBarWidth : Int

    /**
     * Describes the gaps between bars.
     */
    var mBarDistance : Int

    val mWidth : Int
    val mHeight : Int
    val view : View

    /**
     * Registers given renderable object as a part of rendering process.
     * @param renderable Renderable object to be registered
     */
    fun registerRenderable(renderable: Renderable)

    /**
     * Unregisters given renderable object from being a part of rendering process.
     * @param renderable Renderable object to be unregistered
     */
    fun unregisterRenderable(renderable: Renderable)

    /**
     * Registers given gesture context as the touch gestures handler.
     * @param gestureContext Gestures context to be registered
     */
    fun registerGestureContext(gestureContext: GestureContext)

    /**
     * Registers given gesture listener and creates gesture detector using the view properties.
     * @param completeGestureListener Gestures listener to be registered
     * @return Complete gesture detector, initialized with the view properties and given gesture listener
     */
    fun registerGestureDetector(completeGestureListener : CompleteGestureListener) : CompleteGestureDetector
    fun repaint()
}