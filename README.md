# SoundMash 

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

A simple sound processing application for Android.

## Built With

* [AndroidAudioConverter](https://github.com/adrielcafe/AndroidAudioConverter) - Used for audio formats conversion, by Adriel Café;
* [Storage-Chooser](https://github.com/codekidX/storage-chooser) - Used for file exploring, by codekidX;
* [WavFile](http://www.labbookpages.co.uk/audio/javaWavFiles.html) - User for WAV files I/O, created by dr. Andrew Greensted, modified and packed by dr. Geoffrey Challen.

## License
This project is licensed under MIT license, details can be found in the LICENSE.md file.